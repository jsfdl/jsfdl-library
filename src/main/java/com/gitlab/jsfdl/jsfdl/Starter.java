package com.gitlab.jsfdl.jsfdl;

import com.gitlab.jsfdl.jsfdl.bus.EventAdmin;
import com.gitlab.jsfdl.jsfdl.config.ApplicationConfig;
import com.gitlab.jsfdl.jsfdl.config.ReflectionConfig;
import com.gitlab.jsfdl.jsfdl.mvp.dateiMenu.DateiMenuModel;
import com.gitlab.jsfdl.jsfdl.mvp.dateiMenu.DateiMenuMvp;
import com.gitlab.jsfdl.jsfdl.mvp.dateiMenu.DateiMenuPresenter;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadModel;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadPresenter;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp;
import com.gitlab.jsfdl.jsfdl.mvp.mainFrame.SetVisibleMethod;
import com.gitlab.jsfdl.jsfdl.mvp.startstop.StartStopModel;
import com.gitlab.jsfdl.jsfdl.mvp.startstop.StartStopMvp;
import com.gitlab.jsfdl.jsfdl.mvp.startstop.StartStopPresenter;
import com.gitlab.jsfdl.jsfdl.update.VersionInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Global starter class. This class starts the application.
 *
 * @author oli
 */
public class Starter {

    private final static Logger logger = LogManager.getLogger();

    public static void main(String args[]) {

        System.setProperty("log4j.configurationFile", "log4j2.xml");

        final ReflectionConfig config = new ReflectionConfig();
        final ApplicationConfig applicationConfig = new ApplicationConfig();

        VersionInfo versionInfo = applicationConfig.getVersionInfo();
        if(versionInfo.updateAvailable()) {
            logger.info(versionInfo.getUpdateInformations());
        }

        final DateiMenuMvp.Model dateiMenuModel = new DateiMenuModel();
        final DateiMenuMvp.View dateiMenuView = config.getDateiMenuView();
        final DateiMenuPresenter dateiMenuPresenter = new DateiMenuPresenter(dateiMenuModel, dateiMenuView);

        final StartStopModel model = new StartStopModel();
        final StartStopMvp.View startStopView = config.getStartStopPanel();
        final StartStopPresenter startStopPresenter = new StartStopPresenter(startStopView, model);

        final DownloadModel downloadModel = new DownloadModel();
        final DownloadsMvp.View downloadTablePane = config.getDownloadPane();
        final DownloadPresenter downloadsPresenter = new DownloadPresenter(downloadTablePane, downloadModel);

        final EventAdmin eventAdmin = EventAdmin.getInstance();
        eventAdmin.registerPresenter(startStopPresenter);
        eventAdmin.registerPresenter(downloadsPresenter);
        eventAdmin.registerPresenter(dateiMenuPresenter);

        final SetVisibleMethod mainFrameView = config.getMainFrameView();
        mainFrameView.setVisible(dateiMenuView, startStopView, downloadTablePane);

    }

}
