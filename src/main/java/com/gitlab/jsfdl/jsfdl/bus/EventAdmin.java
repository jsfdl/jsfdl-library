package com.gitlab.jsfdl.jsfdl.bus;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class EventAdmin {

    private static EventAdmin instance;

    private final Set<AbstractMessagePresenter> presenters = new HashSet<>();

    public static EventAdmin getInstance() {
        synchronized (EventAdmin.class) {
            if (instance == null) {
                instance = new EventAdmin();
            }
        }
        return instance;
    }

    public synchronized void registerPresenter(AbstractMessagePresenter presenter) {
        presenters.add(presenter);
    }

    public void fireEvent(Event event) {
        final List<AbstractMessagePresenter> collect = presenters.stream()
                .filter(abstractMessagePresenter -> abstractMessagePresenter.eventsToListenFor().contains(event.getClass()))
                .collect(Collectors.toList());

        collect.forEach(abstractMessagePresenter
                -> abstractMessagePresenter.eventFired(event));
    }

    public void fireEvent(EventWithPayload event) {
        final List<AbstractMessagePresenter> collect = presenters.stream()
                .filter(abstractMessagePresenter -> abstractMessagePresenter.eventsToListenFor().contains(event.getClass()))
                .collect(Collectors.toList());

        collect.forEach(abstractMessagePresenter
                -> abstractMessagePresenter.eventFired(event));
    }

}
