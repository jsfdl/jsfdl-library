package com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged;

import com.gitlab.jsfdl.jsfdl.ftp.State;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLEntry;

import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

public class DownloadInformation {

    private final long transferedBytes;
    private final long sizeOfPackageInBytes;
    private final SFDLEntry sfdlEntry;
    private final State state;
    private final Instant startedAt;
    private final long secondsRunning;
    private final long bytePerSecond;

    DownloadInformation(State state, long transferedBytes, SFDLEntry sfdlEntry, Instant startedAt, long secondsRunning, long kbsPerSecond, long sizeOfPackageInBytes) {
        this.transferedBytes = transferedBytes;
        this.sizeOfPackageInBytes = sizeOfPackageInBytes;
        this.sfdlEntry = sfdlEntry;
        this.state = state;
        this.startedAt = startedAt;
        this.secondsRunning = secondsRunning;
        this.bytePerSecond = kbsPerSecond;
    }

    public long getTransferedBytes() {
        return transferedBytes;
    }

    public long getBytePerSecond() {
        return bytePerSecond;
    }

    public long getSizeOfPackageInBytes() {
        return sizeOfPackageInBytes;
    }

    public Optional<Long> getSecondsRunning() {
        return Optional.ofNullable(secondsRunning);
    }

    public SFDLEntry getSfdlEntry() {
        return sfdlEntry;
    }

    public State getState() {
        return state;
    }

    public Optional<Instant> getStartedAt() {
        return Optional.ofNullable(startedAt);
    }

    public int getId() {
        return sfdlEntry.getId();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.sfdlEntry);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DownloadInformation other = (DownloadInformation) obj;
        return Objects.equals(this.sfdlEntry, other.sfdlEntry);
    }
  
  

}
