/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.jsfdl.jsfdl.bus.events.sfdlremoved;

import com.gitlab.jsfdl.jsfdl.bus.EventWithPayload;

/**
 * @author oli
 */
public class SFDLFileRemoved implements EventWithPayload {

    private final int fileId;

    public SFDLFileRemoved(int sfdlFile) {
        this.fileId = sfdlFile;
    }

    @Override
    public Object getPayload() {
        return fileId;
    }

}
