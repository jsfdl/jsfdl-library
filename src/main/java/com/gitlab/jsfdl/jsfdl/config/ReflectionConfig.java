package com.gitlab.jsfdl.jsfdl.config;

import com.gitlab.jsfdl.jsfdl.mvp.dateiMenu.DateiMenuMvp;
import com.gitlab.jsfdl.jsfdl.mvp.dateiMenu.DateiMenuView;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadView;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp;
import com.gitlab.jsfdl.jsfdl.mvp.mainFrame.SetVisibleMethod;
import com.gitlab.jsfdl.jsfdl.mvp.startstop.StartStopMvp;
import com.gitlab.jsfdl.jsfdl.mvp.startstop.StartStopView;
import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.scanner.ScanResult;

import java.util.List;

public class ReflectionConfig {

    private final ScanResult scanResult;

    public ReflectionConfig() {
        FastClasspathScanner scanner = new FastClasspathScanner();
        scanResult = scanner.scan();
    }

    public DateiMenuMvp.View getDateiMenuView() {
        final List<String> namesOfClassesImplementing = scanResult.getNamesOfClassesWithAnnotation(DateiMenuView.class);
        if (namesOfClassesImplementing.size() != 1) {
            throw new NoUIImplementationFoundException();
        }

        try {
            final Class aClass = Class.forName(namesOfClassesImplementing.get(0));

            final Object o = aClass.newInstance();
            if (!(o instanceof DateiMenuMvp.View)) {
                throw new RuntimeException("Found " + aClass + " with DateiMenuView Annotation, but its instance does not implement DateiMenuMvp Interface!");
            }
            return (DateiMenuMvp.View) o;

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public StartStopMvp.View getStartStopPanel() {
        final List<String> namesOfClassesImplementing = scanResult.getNamesOfClassesWithAnnotation(StartStopView.class);
        if (namesOfClassesImplementing.size() != 1) {
            throw new NoUIImplementationFoundException();
        }

        try {
            final Class aClass = Class.forName(namesOfClassesImplementing.get(0));

            final Object o = aClass.newInstance();
            if (!(o instanceof StartStopMvp.View)) {
                throw new RuntimeException("Found " + aClass + " with StartStopView Annotation, but its instance does not implement StartStopMvp Interface!");
            }
            return (StartStopMvp.View) o;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public DownloadsMvp.View getDownloadPane() {
        final List<String> namesOfClassesImplementing = scanResult.getNamesOfClassesWithAnnotation(DownloadView.class);
        if (namesOfClassesImplementing.size() != 1) {
            throw new NoUIImplementationFoundException();
        }

        try {
            final Class aClass = Class.forName(namesOfClassesImplementing.get(0));

            final Object o = aClass.newInstance();
            if (!(o instanceof DownloadsMvp.View)) {
                throw new RuntimeException("Found " + aClass + " with DownloadView Annotation, but its instance does not implement StartStopMvp Interface!");
            }
            return (DownloadsMvp.View) o;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public SetVisibleMethod getMainFrameView() {

        FastClasspathScanner scanner = new FastClasspathScanner("de.nodomain.jsfdl.gui");
        final List<String> namesOfClassesImplementing = scanner.scan().getNamesOfClassesImplementing(SetVisibleMethod.class);

        try {
            final String s = namesOfClassesImplementing.get(0);
            return (SetVisibleMethod) Class.forName(s).newInstance();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


}
