package com.gitlab.jsfdl.jsfdl.ftp.bulkmode;

import com.gitlab.jsfdl.jsfdl.ftp.client.FTPConnection;
import com.gitlab.jsfdl.jsfdl.ftp.client.FTPFileWithFullPath;
import com.gitlab.jsfdl.jsfdl.ftp.download.FTPConnectionPool;
import com.gitlab.jsfdl.jsfdl.ftp.download.FTPDownloadFile;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;


public class TraversalRunner {

    private final FTPConnectionPool ftpConnectionPool;
    private final String packageName;

    private final List<String> directorysToScan = new ArrayList<>();

    public TraversalRunner(String packageName, String sourceDir, FTPConnectionPool ftpConnectionPool) {
        this.ftpConnectionPool = ftpConnectionPool;
        this.packageName = packageName;
        directorysToScan.add(sourceDir);
    }

    public List<FTPDownloadFile> getFilesToDownload() {
        final List<FTPDownloadFile> files = new ArrayList<>();

        while (directorysToScan.listIterator().hasNext()) {

            final FTPConnection freeConnection;
            final String sourceDir;
            synchronized (this) {
                freeConnection = ftpConnectionPool.getFreeConnection();
                final ListIterator<String> stringListIterator = directorysToScan.listIterator();
                sourceDir = stringListIterator.next();
                stringListIterator.remove();

            }

            final List<FTPFileWithFullPath> filesFromDirectory = freeConnection.getFilesFromDirectory(sourceDir);
            ftpConnectionPool.releaseConnection(freeConnection);
            filesFromDirectory.stream().filter(FTPFileWithFullPath::isDirectory).forEach(ftpFileWithFullPath -> directorysToScan.add(ftpFileWithFullPath.getRemoteFileLocation()));
            filesFromDirectory.stream().filter(FTPFileWithFullPath::isFile).forEach(fileWithFullPath -> files.add(new FTPDownloadFile(packageName, sourceDir, fileWithFullPath.getRemoteFileLocation(), fileWithFullPath.getSize())));

        }

        ftpConnectionPool.shutdown();
        return files;
    }


}
