package com.gitlab.jsfdl.jsfdl.ftp.client;

public class DirectoryNotAvailableOnFtp extends Exception {
    public DirectoryNotAvailableOnFtp(String directory) {
        super("Unable to to change to directory on ftp server: " + directory);
    }
}
