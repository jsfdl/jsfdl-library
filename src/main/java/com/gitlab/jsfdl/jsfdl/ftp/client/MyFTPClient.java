package com.gitlab.jsfdl.jsfdl.ftp.client;

import com.gitlab.jsfdl.jsfdl.ftp.download.FTPDownloadFile;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.DownloadOfflineException;
import org.apache.commons.net.ProtocolCommandEvent;
import org.apache.commons.net.ProtocolCommandListener;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPCmd;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MyFTPClient extends FTPClient {
    private final static Logger logger = LogManager.getLogger();

    private final Map<String, Integer> loginCountInfo = new HashMap<>();

    MyFTPClient() {
        addProtocolCommandListener(new ProtocolCommandListener() {
            @Override
            public void protocolCommandSent(ProtocolCommandEvent event) {
                logger.debug(() -> event.getMessage() + ": " + event.getCommand() + " " + event.getReplyCode());
            }

            @Override
            public void protocolReplyReceived(ProtocolCommandEvent event) {
                logger.debug(() -> event.getMessage() + ": " + event.getCommand() + " " + event.getReplyCode());
            }
        });
    }

    @Override
    public boolean login(String username, String password) throws IOException {

        synchronized (MyFTPClient.class) {
            if (loginCountInfo.containsKey(_hostname_)) {
                loginCountInfo.put(_hostname_, loginCountInfo.get(_hostname_) + 1);
            } else
                loginCountInfo.put(_hostname_, 1);
            logger.trace(() -> "LOGIN INFO: Trying login #" + loginCountInfo.get(_hostname_) + " FOR SERVER " + _hostname_);

        }
        return super.login(username, password);
    }

    public long getRemoteFileSize(FTPDownloadFile ftpFile) throws DownloadOfflineException {
        final int sendCommand;
        if (serverSupportsMLST()) {
            sendCommand = sendMLSTCommand(ftpFile);
        } else {
            sendCommand = sendSizeCommand(ftpFile);
        }

        if (sendCommand > 400) {
            logger.error("Tried to get the filesize of " + ftpFile.getRemoteAbsoluteFileLocation() + " and got " + sendCommand + " from server!");
            throw new DownloadOfflineException("Error code for " + ftpFile.getRemoteAbsoluteFileLocation() + ": " + sendCommand);
        }
        return Long.parseLong(getReplyString().split(" ")[1].trim());
    }

    private int sendMLSTCommand(FTPDownloadFile ftpFile) {
        try {
            return mlst(ftpFile.getRemoteAbsoluteFileLocation());
        } catch (IOException e) {
            throw new RuntimeException("error in mlst", e);
        }
    }

    private boolean serverSupportsMLST() {
        try {
            return hasFeature(FTPCmd.MLST.getCommand());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private int sendSizeCommand(FTPDownloadFile ftpFile) {
        try {
            return sendCommand("SIZE", ftpFile.getRemoteAbsoluteFileLocation());
        } catch (IOException e) {
            logger.error(() -> "sent: SIZE " + ftpFile.getRemoteAbsoluteFileLocation(), e);
            throw new RuntimeException("SIZE failed on remote server.");
        }
    }

    public void validateFunctionListFiles() {
        try {
            listFiles();
        } catch (Exception ex) {
            //crapshit implementation of ftp. if the system type is not properly set in the ftp server
            //we will get serious problems retrieving FTPFiles. Therefore, if we encounter such a crappy
            //ftp server, we will try to use "UNIX" as default. Maybe i should write some kind of trial-and-error
            //to get the correct system type.
            logger.debug(ex);
            System.setProperty(FTP_SYSTEM_TYPE, "UNIX");
        }
    }

    public boolean serverSupportsResume() {
        if (!isConnected()) {
            return false;
        }

        try {
            return listHelp().contains("REST");
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }


    public long getRemoteDirectorySize(FTPDownloadFile ftpFile) {
        return 0;
    }
}
