package com.gitlab.jsfdl.jsfdl.ftp.download;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Blacklister {

    private final List<String> blacklist = new ArrayList<>();

    public Blacklister() {
        if (!new File("blacklist_files.txt").exists()) {
            createBlacklistsFile();
        } else {
            File file = new File("blacklist_files.txt");
            try {
                Scanner scanner = new Scanner(file);
                while (scanner.hasNext()) {
                    String line = scanner.nextLine();
                    if (!line.trim().startsWith("#")) {
                        blacklist.add(line);
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void createBlacklistsFile() {
        File file = new File("blacklist_files.txt");

        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)))) {
            out.println("#regex based blacklist for files. each file/regex listed here, will not be downloaded!");
            out.println("#for example, to exclude all *.exe files, uncomment the next line:");
            out.println("#.*\\.exe");
            out.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean contains(String remotePathOrFilename) {
        for (String entry : blacklist) {
            if (remotePathOrFilename.matches(entry)) {
                return true;
            }
        }
        return false;
    }
}
