package com.gitlab.jsfdl.jsfdl.ftp.download;

import com.gitlab.jsfdl.jsfdl.ftp.client.FTPConnection;

import java.io.File;
import java.util.concurrent.Callable;

public class DownloadJob implements Callable<Void> {

    private final String remoteFile;
    private final File localFile;
    private final FTPConnection ftpConnection;

    public DownloadJob(FTPConnection ftpConnection, String remoteFile, File localFile) {
        this.remoteFile = remoteFile;
        this.localFile = localFile;
        this.ftpConnection = ftpConnection;
    }

    @Override
    public Void call() {
        ftpConnection.download(remoteFile, localFile);
        return null;
    }
}
