package com.gitlab.jsfdl.jsfdl.ftp.download;

import com.gitlab.jsfdl.jsfdl.bus.EventAdmin;
import com.gitlab.jsfdl.jsfdl.ftp.client.FTPConnection;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLEntry;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.DownloadOfflineException;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.ServerOfflineException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.nio.file.Files;
import java.util.concurrent.Callable;

public class DownloadRunnable implements Callable {

    private final Logger logger = LogManager.getLogger();

    private final EventAdmin eventAdmin = EventAdmin.getInstance();
    private final String destDir;
    private final FTPDownloadFile ftpFile;
    private final SFDLEntry sfdlEntry;
    private final SFDLFileDownloader parent;
    private final FTPConnection ftpConnection;

    public DownloadRunnable(SFDLFileDownloader parent, SFDLEntry sfdlEntry, String destDir, FTPDownloadFile ftpFile, FTPConnection ftpConnection) {
        this.parent = parent;
        this.destDir = destDir;
        this.ftpFile = ftpFile;
        this.sfdlEntry = sfdlEntry;
        this.ftpConnection = ftpConnection;
    }

    @Override
    public String toString() {
        return "DownloadRunnable{" + "eventAdmin=" + eventAdmin + ", destDir="
                + destDir + ", ftpFile=" + ftpFile
                + ", sfdlFile=" + sfdlEntry + ", parent=" + parent + '}';
    }

    @Override
    public Object call() throws DownloadOfflineException, InterruptedException {
        return downloadFile();
    }

    private Object downloadFile() throws DownloadOfflineException, ServerOfflineException, InterruptedException {
        logger.trace(() -> "downloadFile()");
        try {
            ftpConnection.initFTPClient();
        } catch (ConnectException x) {
            throw new ServerOfflineException();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ftpConnection.validateFunctionListFiles();

        final File outFile = createOutFile();
        synchronized (this) {
            logger.trace(() -> "!!! SYNCHRONIZED !!! " + Thread.currentThread().getId());
            if (!outFile.getParentFile().exists()) {
                try {
                    Files.createDirectories(outFile.getParentFile().toPath());
                } catch (IOException ex) {
                    logger.error(() -> "Unable to create dir: " + outFile.getParentFile().getAbsolutePath());
                    throw new RuntimeException("Unable to create dir: " + outFile.getParentFile().getAbsolutePath());
                }
            }
        }
        logger.trace(() -> "!!! SYNCHRONIZED FINISHED " + Thread.currentThread().getId());

        logger.debug(() -> "checking if the file " + outFile.getAbsolutePath() + " already exists");
        if (outFile.exists()) {
            logger.debug(() -> "the file " + outFile.getAbsolutePath() + " already exists");

            if (ftpConnection.serverSupportsResume()) {
                logger.debug(() -> "server supports resume!");
                resumeWriteToFile(ftpConnection, outFile);
            } else {
                logger.info("Server doesnt support resume. need to restart :(");
                writeToFile(ftpConnection, outFile);
                ftpConnection.closeConnection();
            }
        } else {
            //file doesnt exist yet
            writeToFile(ftpConnection, outFile);
            ftpConnection.closeConnection();
        }
        return null;
    }

    private File createOutFile() {
        logger.trace(() -> "createOutFile()");
        final File file = new File(destDir, ftpFile.getRemoteAbsoluteFileLocation().replaceFirst(ftpFile.getSourceDirectoryOnServer(), ""));
        logger.debug("created " + file.getAbsolutePath());
        return file;
    }

    private void resumeWriteToFile(FTPConnection ftpConnection, File outFile) throws InterruptedException {

        logger.debug(() -> "resume write on " + outFile.getAbsolutePath());
        //we need to resume, if possible
        //todo: extract method
        final long offsetRemoteFile;
        try {
            offsetRemoteFile = this.ftpConnection.getSizeOfFile(ftpFile.getRemoteAbsoluteFileLocation());
        } catch (DownloadOfflineException e) {
            throw e;
        }
        logger.debug(() -> "FTP: SIZE " + ftpFile + ": " + offsetRemoteFile + " byte.");

        //extract method
        final long offsetLocalFile = outFile.length();
        logger.debug(() -> "size of the file on the harddrive: " + offsetLocalFile + " byte.");

        //extract method
        if (offsetLocalFile == offsetRemoteFile) {
            logger.debug(() -> outFile + " got transfered completly already, skipping.");
            parent.addTransferedBytesWithoutDownloading(outFile.length());
            return;
        }

        this.ftpConnection.setRestartOffset(offsetLocalFile);
        parent.addTransferedBytesWithoutDownloading(offsetLocalFile);

        appendToFile(ftpConnection, outFile);
    }

    private void writeToFile(FTPConnection ftpConnection, File outFile) throws InterruptedException {

        String status = this.ftpConnection.getStatus(ftpFile.getRemoteAbsoluteFileLocation());
        logger.trace(() -> "Status of file " + ftpFile.getRemoteAbsoluteFileLocation() + " is " + status);

        try (final FileOutputStream fos = new FileOutputStream(outFile);
             final InputStream retrieveFileStream = ftpConnection.retrieveFileStream(ftpFile.getRemoteAbsoluteFileLocation())) {
            retrieveAndWrite(ftpConnection, outFile, fos, retrieveFileStream);
        } catch (IOException ex) {
            logger.error("Error retrieving bytes in transfer.", ex);
            throw new RuntimeException("Error retrieving bytes in transfer: " + ex.getLocalizedMessage());
        }
    }

    private void appendToFile(FTPConnection ftpConnection, File outFile) throws InterruptedException {

        String status = this.ftpConnection.getStatus(ftpFile.getRemoteAbsoluteFileLocation());
        logger.trace(() -> "Status of file " + ftpFile.getRemoteAbsoluteFileLocation() + " is " + status);

        try (final FileOutputStream fos = new FileOutputStream(outFile, true);
             final InputStream retrieveFileStream = ftpConnection.retrieveFileStream(ftpFile.getRemoteAbsoluteFileLocation())) {
            retrieveAndWrite(ftpConnection, outFile, fos, retrieveFileStream);
        } catch (IOException ex) {
            logger.error("Error retrieving bytes in transfer.", ex);
            throw new RuntimeException("Error retrieving bytes in transfer: " + ex.getLocalizedMessage());
        }
    }

    private void retrieveAndWrite(FTPConnection ftpConnection, File outFile, FileOutputStream fos, InputStream retrieveFileStream) throws IOException, InterruptedException {
        logger.debug(() -> "retrieving FileInputStream from FTP Server for file " + outFile);
        byte b[] = new byte[8192]; //todo: would be nice to have such stuff in the appl conf so users can config it
        long bytesWritten = 0;
        int len;
        while ((len = retrieveFileStream.read(b)) != -1) {
            if (Thread.interrupted()) {
                logger.debug(() -> "thread got interrupted!");
                ftpConnection.abort();
                ftpConnection.logout();
                fos.flush();
                throw new InterruptedException();
            }
            fos.write(b, 0, len);
            bytesWritten += len;
            parent.addTransferedBytesWithDownloading(len);
        }

        final long tmp = bytesWritten;
        logger.debug(() -> "Finished reading the InputStream. Retrieved " + tmp + " bytes.");
        fos.flush();
    }

}
