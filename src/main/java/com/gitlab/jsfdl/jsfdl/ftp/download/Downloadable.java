package com.gitlab.jsfdl.jsfdl.ftp.download;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

public interface Downloadable extends Callable<Void> {

    @Override
    default Void call() throws IOException, InterruptedException, ExecutionException {
        return download();
    }

    Void download() throws IOException, InterruptedException, ExecutionException;
}
