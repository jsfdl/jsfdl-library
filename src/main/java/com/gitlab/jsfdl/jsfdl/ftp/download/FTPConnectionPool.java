package com.gitlab.jsfdl.jsfdl.ftp.download;

import com.gitlab.jsfdl.jsfdl.ftp.client.FTPConnection;
import com.gitlab.jsfdl.jsfdl.ftp.client.FTPFactory;
import com.gitlab.jsfdl.jsfdl.sfdl.model.ConnectionInfo;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.ServerOfflineException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FTPConnectionPool {

    private final Map<Integer, FTPConnection> freeConnections = new HashMap<>();
    private final int maxThreads;
    private final FTPFactory ftpFactory;
    private final ConnectionInfo connectionInfo;
    private boolean initialized = false;

    public FTPConnectionPool(int maxThreads, FTPFactory ftpFactory, ConnectionInfo connectionInfo) {
        this.maxThreads = maxThreads;
        this.ftpFactory = ftpFactory;
        this.connectionInfo = connectionInfo;
    }

    public synchronized void initialize() {
        if (!initialized) {
            for (int i = 0; i < maxThreads; i++) {
                final FTPConnection ftpConnection = new FTPConnection(ftpFactory, connectionInfo);
                try {
                    ftpConnection.initFTPClient();
                } catch (IOException e) {
                    throw new ServerOfflineException();
                }
                freeConnections.put(ftpConnection.hashCode(), ftpConnection);
            }
            initialized = true;
        }
    }

    public synchronized FTPConnection getFreeConnection() {
        final Iterator<Integer> iterator;
        if (!initialized) {
            throw new RuntimeException("Initialize first!");
        }

        iterator = freeConnections.keySet().iterator();
        while (!iterator.hasNext()) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        final Integer next = iterator.next();
        return freeConnections.remove(next);
    }

    public void releaseConnection(FTPConnection ftpConnection) {
        if (!initialized) {
            throw new RuntimeException("Initialize first!");
        }
        freeConnections.put(ftpConnection.hashCode(), ftpConnection);
    }

    public int connections() {
        return freeConnections.size();
    }

    public void shutdown() {
        if (!initialized) {
            throw new RuntimeException("Initialize first!");
        }
        freeConnections.values().forEach(FTPConnection::closeConnection);
    }
}
