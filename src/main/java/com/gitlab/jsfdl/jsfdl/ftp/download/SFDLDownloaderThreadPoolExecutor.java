package com.gitlab.jsfdl.jsfdl.ftp.download;

import com.gitlab.jsfdl.jsfdl.ftp.State;
import com.gitlab.jsfdl.jsfdl.ftp.WorkerThreadFactory;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLFile;

import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.stream.Collectors;

public class SFDLDownloaderThreadPoolExecutor extends ScheduledThreadPoolExecutor {

    private final Map<SFDLFileDownloader, Future<?>> registeredTasks = new HashMap<>();

    public SFDLDownloaderThreadPoolExecutor(int threadCount) {
        super(threadCount, new WorkerThreadFactory("global-sfdldownload"));
    }

    public void addRegisteredTasks(Set<SFDLFileDownloader> registeredTasks) {
        registeredTasks.forEach(this::register);
    }

    @Override
    public List<Runnable> shutdownNow() {
        super.shutdownNow();
        registeredTasks.keySet()
                .stream()
                .filter((t) -> t.getState().equals(State.DOWNLOADING))
                .forEach(SFDLFileDownloader::abortAllDownloads);
        return Collections.EMPTY_LIST;
    }

    public void register(SFDLFileDownloader sfdlFileDownloader) {
        if (registeredTasks.keySet().stream().anyMatch((t) -> t.getSfdlEntry().getId() == sfdlFileDownloader.getSfdlEntry().getId())) {
            return;
        }

        final RunnableFuture<Void> sfdlFileRunnableFuture = newTaskFor(sfdlFileDownloader);
        registeredTasks.put(sfdlFileDownloader, sfdlFileRunnableFuture);
    }

    public Set<SFDLFileDownloader> getRegisteredTasks() {
        return registeredTasks.keySet();
    }

    public void startDownloadFor(SFDLFile sfdlFile) {
        final Optional<SFDLFileDownloader> first = registeredTasks.keySet().stream().filter(
                sfdlFileDownloader -> sfdlFileDownloader.getSfdlEntry().equals(sfdlFile)).findFirst();
        first.ifPresent(this::submit);
    }

    public Set<SFDLFileDownloader> getRunningDownloads() {
        return Collections.unmodifiableSet(
                registeredTasks.keySet()
                        .stream()
                        .filter((t) -> t.getState().equals(State.DOWNLOADING))
                        .collect(Collectors.toSet())
        );
    }

    public synchronized List<Integer> startAllDownloads() {
        List<Integer> started = new ArrayList<>();
        registeredTasks.keySet()
                .stream()
                .filter(
                        (t) -> t.getState().equals(State.WAITING) || t.getState().equals(State.ABORTED)
                )
                .forEach((t) -> {
                    Future<Void> submitted = submit(t);
                    started.add(t.getSfdlEntry().getId());
                });
        return started;
    }

    public void removeSfdl(int payload) {
        Optional<SFDLFileDownloader> findAny = registeredTasks.keySet().stream().filter((t) -> t.getSfdlEntry().getId() == payload).findAny();
        if (findAny.isPresent()) {
            registeredTasks.remove(findAny.get());
        }
    }
}
