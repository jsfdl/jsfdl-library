package com.gitlab.jsfdl.jsfdl.ftp.download;

import com.gitlab.jsfdl.jsfdl.app.JSFDLConfig;
import com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged.DownloadInfoBuilderForSFDL;
import com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged.DownloadInformation;
import com.gitlab.jsfdl.jsfdl.ftp.State;
import com.gitlab.jsfdl.jsfdl.ftp.TimeWatch;
import com.gitlab.jsfdl.jsfdl.ftp.WorkerThreadFactory;
import com.gitlab.jsfdl.jsfdl.ftp.bulkmode.TraversalRunner;
import com.gitlab.jsfdl.jsfdl.ftp.bulkmode.TraversalRunnerJob;
import com.gitlab.jsfdl.jsfdl.ftp.client.DirectoryNotAvailableOnFtp;
import com.gitlab.jsfdl.jsfdl.ftp.client.FTPConnection;
import com.gitlab.jsfdl.jsfdl.ftp.client.FTPFactory;
import com.gitlab.jsfdl.jsfdl.ftp.listmode.FTPValidator;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp.DownloadChangedListener;
import com.gitlab.jsfdl.jsfdl.sfdl.model.ConnectionInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.model.FileInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLEntry;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.AuthenticationException;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.DownloadOfflineException;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.ServerOfflineException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;

import static com.gitlab.jsfdl.jsfdl.ftp.State.*;
import static java.util.Objects.requireNonNull;

/**
 * Specific waitForDownloads manager for one package (e.g. a movie) which uses the thread count,
 * given in the .xml
 */
public class SFDLFileDownloader implements Downloadable {

    private final Logger logger = LogManager.getLogger();
    private final SFDLEntry sfdlEntry;
    private final JSFDLConfig config;
    private final List<DownloadsMvp.DownloadChangedListener> listeners = new ArrayList<>();
    private final ScheduledExecutorService scheduledDownloadExecutorService;
    private final FTPValidator ftpValidator;
    private final FTPFactory ftpFactory;
    private State state = State.WAITING;
    private long transferedBytesWithoutDownloading = 0;
    private long transferedBytesInDownloading = 0;
    private long oldTransferedBytes = 0;
    private Instant startedAt;
    private long secondsRunning = 1;
    private long sizeOfPackageInBytes = 0;
    private TimeWatch timeWatch = TimeWatch.start();

    public SFDLFileDownloader(SFDLEntry sfdlEntry, JSFDLConfig config, FTPValidator ftpValidator, FTPFactory ftpFactory) {
        requireNonNull(this.sfdlEntry = sfdlEntry);
        requireNonNull(this.config = config);
        requireNonNull(scheduledDownloadExecutorService = new ScheduledThreadPoolExecutor(sfdlEntry.getMaxThreads(), new WorkerThreadFactory("download")));
        requireNonNull(this.ftpValidator = ftpValidator);
        requireNonNull(this.ftpFactory = ftpFactory);
    }

    public void addDownloadChangedListener(DownloadChangedListener listener) {
        logger.trace(() -> "Added DownloadChangedListener " + listener);
        if (listeners.contains(listener)) {
            return;
        }
        listeners.add(listener);
    }

    public SFDLEntry getSfdlEntry() {
        return sfdlEntry;
    }

    private void downloadTo(String destinationDir) {
        logger.trace(() -> "downloadTo " + destinationDir);
        final List<Future> submittedDownloadTasks = initDownloads(destinationDir);
        waitForDownloads(submittedDownloadTasks);
    }

    private void waitForDownloads(List<Future> submittedDownloadTasks) {
        logger.trace(() -> "waiting for downloads");
        for (Future t : submittedDownloadTasks) {
            try {
                t.get();
            } catch (InterruptedException ex) {
                submittedDownloadTasks.forEach((s) -> s.cancel(true));
                fireTransfereAborted();
                logger.error(ex);
            } catch (ExecutionException ex) {
                fireTransfereError();
                logger.error(ex);
                ex.printStackTrace();
                return;
            } catch (DownloadOfflineException ex) {
                //if we encounter one missing part, we abort them all
                submittedDownloadTasks.forEach((s) -> s.cancel(true));
                return;
            }
        }

        logger.trace(() -> "waiting until waitForDownloads finished...");
        try {
            while (!scheduledDownloadExecutorService.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS)) {
                throw new RuntimeException("You waited very very very long...");
            }
        } catch (InterruptedException ex) {
            fireTransfereAborted();
            return;
        }

        if (scheduledDownloadExecutorService.isTerminated()) {
            logger.debug(() -> "waitForDownloads finished.");
            fireDownloadFinished();
        }

    }

    private List<Future> initDownloads(String destinationDir) {
        logger.trace(() -> "initDownloads " + destinationDir);
        final String fixDestinationDir;
        if (destinationDir == null) {
            fixDestinationDir = ".";
        } else {
            fixDestinationDir = destinationDir;
        }

        logger.trace(() -> "downloadTo(" + fixDestinationDir + ")");
        final File outputDir = new File(fixDestinationDir + "/" + sfdlEntry.getDisplayName());
        final FTPDownload ftpDownload = getFtpDownloads();

        ftpDownload.getDownloadFiles().forEach((t) -> {
            logger.debug(() -> "File to waitForDownloads: " + t);
        });

        sizeOfPackageInBytes = ftpDownload.getPackageSize();

        startedAt = Instant.now();

        logger.debug(() -> "created sheduled executor service (for downloads) with " + sfdlEntry.getMaxThreads() + " threads.");
        final List<Future> submittedDownloadTasks = new ArrayList<>();
        for (final FTPDownloadFile file : ftpDownload.getDownloadFiles()) {
            logger.debug(() -> "creating DownloadRunnable for " + file);
            FTPConnection ftpConnection = new FTPConnection(ftpFactory, sfdlEntry.connectionInfo());
            final DownloadRunnable downloadRunnable = new DownloadRunnable(this, sfdlEntry, outputDir.getAbsolutePath(), file, ftpConnection);
            logger.debug(() -> "created: " + downloadRunnable);
            final Future submit = scheduledDownloadExecutorService.submit(downloadRunnable);
            submittedDownloadTasks.add(submit);
        }
        state = State.DOWNLOADING;
        logger.debug(() -> "closing waitForDownloads manager to prevent new downloads.");
        scheduledDownloadExecutorService.shutdown();
        return submittedDownloadTasks;
    }

    private FTPDownload getFtpDownloads() {
        final FTPDownload ftpDownload;
        if (sfdlEntry.isBulkfolder()) {
            try {
                ftpDownload = getBulkModeDownloads();
            } catch (InterruptedException ex) {
                fireTransfereAborted();
                throw new RuntimeException(ex);
            }
        } else {
            ftpDownload = getListModeDownloads();
            List<FTPValidator.VALIDATION_STATE> errors = ftpValidator.validate(sfdlEntry);
            if (errors.size() > 0) { //TODO: separate the errors when validate() returns all states
                fireDownloadOffline();
                throw new RuntimeException("Error(s) occured during validating files in listMode!: " + errors.toString());
            }
        }

        Blacklister blacklister = config.getBlacklister();
        Iterator it = ftpDownload.getDownloadFiles().iterator();
        while (it.hasNext()) {
            FTPDownloadFile ftpDownloadFile = (FTPDownloadFile) it.next();
            if (blacklister.contains(ftpDownloadFile.getRemoteAbsoluteFileLocation())) {
                it.remove();
            }
        }

        return ftpDownload;
    }

    private FTPDownload getListModeDownloads() {
        final FTPDownload ftpDownload = new FTPDownload();
        final List<FileInfo> fileList = sfdlEntry.getFileInfos();
        fileList.forEach((t) -> {
            ftpDownload.addFile(new FTPDownloadFile(t.getPackageName(), t.getDirectoryRoot(), t.getFileFullPath(), t.getFileSize()));
        });
        return ftpDownload;
    }

    private FTPDownload getBulkModeDownloads() throws InterruptedException {
        final FTPDownload ftpDownload = new FTPDownload();
        final String sourceDir = sfdlEntry.getBulkRemoteSourceDir();

        //now retrieve all files, recursive
        ScheduledThreadPoolExecutor traversalScheduledExecutor = new ScheduledThreadPoolExecutor(1, new WorkerThreadFactory("traversalrunner")) {
            @Override
            protected void beforeExecute(Thread t, Runnable r) {
                analyzingFile();
            }

            @Override
            protected void afterExecute(Runnable r, Throwable t) {
                analyzingFileFinished();
            }
        };

        logger.debug(() -> "Creating Traversalrunner.");
        final FTPConnectionPool ftpConnectionPool = getFtpConnectionPool(sfdlEntry.getMaxThreads(), ftpFactory, sfdlEntry.connectionInfo());

        final TraversalRunner runner = new TraversalRunner(sfdlEntry.getPackageName(), sourceDir, ftpConnectionPool);
        TraversalRunnerJob job = new TraversalRunnerJob(runner);
        //we use our own executor service for looping through the filelist

        try {
            List<FTPDownloadFile> remoteFilesRaw = traversalScheduledExecutor.submit(job).get();
            traversalScheduledExecutor.shutdown();
            remoteFilesRaw.forEach(ftpDownload::addFile);
        } catch (InterruptedException | ExecutionException ex) {
            logger.error(ex);
            if (ex.getCause().getClass().equals(ServerOfflineException.class)) {
                fireServerOffline();
            } else if (ex.getCause().getClass().equals(DownloadOfflineException.class)) {
                fireDownloadOffline();
            } else if (ex.getCause().getClass().equals(AuthenticationException.class)) {
                fireAuthenticationException();
            } else if (ex.getCause().getClass().equals(DirectoryNotAvailableOnFtp.class)) {
                fireDirectoryNotAvailableException();
            } else
                fireUnknownError();

            throw new RuntimeException(ex);
        }

        logger.debug(() -> "waiting for traversal runner to finish.");
        while (!traversalScheduledExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS)) {
            throw new RuntimeException("You waited very very very long...");
        }
        logger.debug(() -> "traversalrunner finished graceful.");
        return ftpDownload;
    }

    protected FTPConnectionPool getFtpConnectionPool(int maxThreads, FTPFactory ftpFactory, ConnectionInfo connectionInfo) {
        try {
            FTPConnectionPool ftpConnectionPool = new FTPConnectionPool(sfdlEntry.getMaxThreads(), ftpFactory, sfdlEntry.connectionInfo());
            ftpConnectionPool.initialize();
            return ftpConnectionPool;
        } catch (ServerOfflineException ex) {
            fireServerOffline();
            throw new RuntimeException("Server offline... aborting.");
        }

    }

    public void abortAllDownloads() {
        if (scheduledDownloadExecutorService != null) {
            scheduledDownloadExecutorService.shutdownNow();
            try {
                scheduledDownloadExecutorService.awaitTermination(10, TimeUnit.SECONDS);
            } catch (InterruptedException ex) {
                logger.error(ex);
            }
        }
//        reset();
        fireTransfereAborted();
    }

    /**
     * So we dont mess up the speedy after a pause or during recover. This would increase the kb/s
     * which is wrong.
     *
     * @param bytes
     */
    protected void addTransferedBytesWithoutDownloading(long bytes) {
        transferedBytesWithoutDownloading += bytes;
    }

    protected void addTransferedBytesWithDownloading(long bytes) {

        synchronized (SFDLFileDownloader.class) {
            if (timeWatch.oneSecondPassed()) {
                fireTransferedData(oldTransferedBytes);
                oldTransferedBytes = 0;
                timeWatch.reset();
            }
        }

        oldTransferedBytes += bytes;

        transferedBytesInDownloading += bytes;
    }

    private void fireTransferedData(long bytesPerSecond) {

        final DownloadInformation build = new DownloadInfoBuilderForSFDL(sfdlEntry)
                .withSpeed(bytesPerSecond)
                .inState(getState() == ERROR_PARTIAL_DOWNLOAD ? ERROR_PARTIAL_DOWNLOAD : State.DOWNLOADING)
                .runningForSeconds(secondsRunning)
                .withPackageSize(sizeOfPackageInBytes)
                .withTransfered(transferedBytesInDownloading + transferedBytesWithoutDownloading)
                .build();
        listeners.forEach((t) -> t.downloadChanged(build)
        );
    }

    private void fireTransfereAborted() {
        this.state = State.ABORTED;
        final DownloadInformation build = new DownloadInfoBuilderForSFDL(sfdlEntry)
                .inState(State.ABORTED)
                .withTransfered(transferedBytesWithoutDownloading + transferedBytesInDownloading)
                .withPackageSize(sizeOfPackageInBytes)//todo: this is a onetime information, get rid of that, bad design!!
                .startedAt(startedAt)
                .runningForSeconds(secondsRunning)
                .build();
        listeners.forEach((t) -> t.downloadChanged(build));
    }

    private void fireDownloadFinished() {
        if (state == ERROR_PARTIAL_DOWNLOAD)
            state = State.PARTIAL_DOWNLOAD_FINISHED;
        else
            this.state = State.FINISHED;
        final DownloadInformation build = new DownloadInfoBuilderForSFDL(sfdlEntry)
                .inState(State.FINISHED)
                .withTransfered(transferedBytesWithoutDownloading + transferedBytesInDownloading)
                .startedAt(startedAt)
                .withPackageSize(sizeOfPackageInBytes)//todo: this is a onetime information, get rid of that, bad design!!
                .runningForSeconds(secondsRunning)
                .build();
        listeners.forEach((t) -> t.downloadChanged(build));
    }

    public State getState() {
        return state;
    }

    @Override
    public Void download() {
        downloadTo(config.getDestinationDir());
        return null;
    }

    private void analyzingFile() {
        this.state = ANALYZING;
        final DownloadInformation build = new DownloadInfoBuilderForSFDL(sfdlEntry).inState(ANALYZING).build();
        listeners.forEach((t) -> t.downloadChanged(build));
    }

    private void analyzingFileFinished() {
        this.state = ANALYZING_FINISHED;
        final DownloadInformation build = new DownloadInfoBuilderForSFDL(sfdlEntry).inState(ANALYZING).build();
        listeners.forEach((t) -> t.downloadChanged(build));
    }

    private void fireTransfereError() {
        this.state = State.ERROR;
        final DownloadInformation build = new DownloadInfoBuilderForSFDL(sfdlEntry).inState(State.ERROR).build();
        listeners.forEach((t) -> t.downloadChanged(build));
    }

    private void fireDownloadOffline() {
        this.state = State.OFFLINE;
        final DownloadInformation build = new DownloadInfoBuilderForSFDL(sfdlEntry).inState(State.OFFLINE).build();
        listeners.forEach((t) -> t.downloadChanged(build));
    }

    private void fireServerOffline() {
        this.state = State.SERVER_DOWN;
        final DownloadInformation build = new DownloadInfoBuilderForSFDL(sfdlEntry).inState(State.SERVER_DOWN).build();
        listeners.forEach((t) -> t.downloadChanged(build));
    }

    private void fireAuthenticationException() {
        this.state = State.AUTHENTICATION;
        final DownloadInformation build = new DownloadInfoBuilderForSFDL(sfdlEntry).inState(State.AUTHENTICATION).build();
        listeners.forEach((t) -> t.downloadChanged(build));
    }

    private void fireDirectoryNotAvailableException() {
        this.state = State.ERROR_PARTIAL_DOWNLOAD;
        final DownloadInformation build = new DownloadInfoBuilderForSFDL(sfdlEntry).inState(State.ERROR_PARTIAL_DOWNLOAD).build();
        listeners.forEach((t) -> t.downloadChanged(build));
    }

    private void fireUnknownError() {
        this.state = State.ERROR;
        final DownloadInformation build = new DownloadInfoBuilderForSFDL(sfdlEntry).inState(State.ERROR).build();
        listeners.forEach((t) -> t.downloadChanged(build));
    }

}
