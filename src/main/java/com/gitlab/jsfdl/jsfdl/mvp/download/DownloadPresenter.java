package com.gitlab.jsfdl.jsfdl.mvp.download;

import com.gitlab.jsfdl.jsfdl.bus.AbstractMessagePresenter;
import com.gitlab.jsfdl.jsfdl.bus.Event;
import com.gitlab.jsfdl.jsfdl.bus.EventWithPayload;
import com.gitlab.jsfdl.jsfdl.bus.events.abortalldownloads.AbortAllDownloads;
import com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged.DownloadChangedEvent;
import com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged.DownloadInformation;
import com.gitlab.jsfdl.jsfdl.bus.events.sfdladded.SFDLFileAdded;
import com.gitlab.jsfdl.jsfdl.bus.events.sfdladded.SFDLFileAddedInformation;
import com.gitlab.jsfdl.jsfdl.bus.events.sfdlremoved.SFDLFileRemoved;
import com.gitlab.jsfdl.jsfdl.bus.events.startalldownloads.StartAllDownloads;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DownloadPresenter extends AbstractMessagePresenter {

    private final DownloadsMvp.View view;
    private final DownloadsMvp.Model model;

    public DownloadPresenter(DownloadsMvp.View view, DownloadsMvp.Model model) {
        this.view = view;
        this.model = model;

        view.registerSfdlFileRemovedListener(this::sfdlFileRemoved);
//        model.addSfdlRemoved()
    }

    private void sfdlFileRemoved(int file) {
        model.removeSfdl(file);
    }

    public void downloadChanged(DownloadInformation info) {
        view.downloadChangedForSFDL(info);
    }

    @Override
    public Set<Class> eventsToListenFor() {
        return new HashSet<>(Arrays.asList(
                AbortAllDownloads.class,
                StartAllDownloads.class,
                SFDLFileAdded.class,
                DownloadChangedEvent.class));
    }

    @Override
    public void eventFired(Event event) {
        if (event instanceof StartAllDownloads) {
            List<Integer> started = model.startAllDownloads();
            view.allDownloadsStarted(started);
        } else if (event instanceof AbortAllDownloads) {
            List<Integer> abortAllDownloads = model.abortAllDownloads();
            view.allDownloadsAborted(abortAllDownloads);
        }
    }

    @Override
    public void eventFired(EventWithPayload event) {
        if (event instanceof SFDLFileAdded) {
            model.addSfdlToDownload((SFDLFileAddedInformation) event.getPayload());
            model.addDownloadChangedInformationListener(((SFDLFileAddedInformation) event.getPayload()).getId(),
                    view.getDownloadChangedListenerFor(((SFDLFileAddedInformation) event.getPayload()).getId()));
            view.addedSFDLToDownload((SFDLFileAddedInformation) event.getPayload());
        } else if (event instanceof SFDLFileRemoved) {
//            model.removeSfdl((Integer) event.getPayload());
//            view.removedSfdl((Integer) event.getPayload());
        }
    }
}
