package com.gitlab.jsfdl.jsfdl.mvp.download;

import com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged.DownloadChangedEvent;
import com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged.DownloadInformation;
import com.gitlab.jsfdl.jsfdl.bus.events.sfdladded.SFDLFileAddedInformation;
import com.gitlab.jsfdl.jsfdl.ftp.download.SFDLFileDownloader;

import java.util.List;
import java.util.Set;

public interface DownloadsMvp {

    interface Model {

        void addSfdlToDownload(SFDLFileAddedInformation info);

        void addDownloadChangedInformationListener(int id, DownloadsMvp.DownloadChangedListener listener);

        Set<SFDLFileDownloader> getRunningDownloads();

        List<Integer> startAllDownloads();

        List<Integer> abortAllDownloads();

        void downloadChangedFor(DownloadChangedEvent downloadChangedEvent);

        void removeSfdl(int payload);

    }

    interface View {
        //register the actions for the popup menu here

        void addedSFDLToDownload(SFDLFileAddedInformation file);

        void allDownloadsStarted(List<Integer> started);

        void allDownloadsAborted(List<Integer> aborted);

        void downloadChangedForSFDL(DownloadInformation info);

        void removedSfdl(int sfdlFile);

        DownloadChangedListener getDownloadChangedListenerFor(int id);

        void registerSfdlFileRemovedListener(SfdlFileRemovedListener listener);
    }

    interface SfdlFileRemovedListener {
        void removedSfdl(int id);
    }

    interface DownloadChangedListener {

        void downloadChanged(DownloadInformation info);
    }
}
