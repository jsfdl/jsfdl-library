package com.gitlab.jsfdl.jsfdl.mvp.mainFrame;

import com.gitlab.jsfdl.jsfdl.mvp.dateiMenu.DateiMenuMvp;
import com.gitlab.jsfdl.jsfdl.mvp.download.DownloadsMvp;
import com.gitlab.jsfdl.jsfdl.mvp.startstop.StartStopMvp;

public interface SetVisibleMethod {

    void setVisible(DateiMenuMvp.View dateiMenuView, StartStopMvp.View startStopView, DownloadsMvp.View jtable);

}
