package com.gitlab.jsfdl.jsfdl.mvp.startstop;

import com.gitlab.jsfdl.jsfdl.bus.AbstractMessagePresenter;
import com.gitlab.jsfdl.jsfdl.bus.Event;
import com.gitlab.jsfdl.jsfdl.bus.EventAdmin;
import com.gitlab.jsfdl.jsfdl.bus.EventWithPayload;
import com.gitlab.jsfdl.jsfdl.bus.events.abortalldownloads.AbortAllDownloads;
import com.gitlab.jsfdl.jsfdl.bus.events.startalldownloads.StartAllDownloads;

import java.util.Collections;
import java.util.Set;

public class StartStopPresenter extends AbstractMessagePresenter {

    private final StartStopMvp.View view;
    private final StartStopMvp.Model model;
    private final EventAdmin eventAdmin = EventAdmin.getInstance();

    public StartStopPresenter(StartStopMvp.View view, StartStopMvp.Model model) {
        this.view = view;
        this.model = model;

        //invoked when the user clicks "Start Download"
        view.registerStartDownloadListener(this::onStartDownloadClicked);

        //invoked when the user clicks "Cancel" or closes the window
        view.registerCancelDownloadListener(this::onCancelDownloadClicked);
    }

    public void onCancelDownloadClicked() {
        eventAdmin.fireEvent(new AbortAllDownloads());
    }

    public void onStartDownloadClicked() {
        eventAdmin.fireEvent(new StartAllDownloads());
    }

    @Override
    public Set<Class> eventsToListenFor() {
        return Collections.emptySet();
    }

    @Override
    public void eventFired(Event event) {

    }

    @Override
    public void eventFired(EventWithPayload eventWithPayload) {

    }
}
