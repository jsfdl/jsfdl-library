package com.gitlab.jsfdl.jsfdl.sfdl.model;

import com.gitlab.jsfdl.jsfdl.sfdl.SFDLCryptor;

public class ConnectionInfo {
    private final int port;
    private String host;
    private String username;
    private String password;

    public ConnectionInfo(String host, int port, String username, String password) {
        this.host = host;
        this.port = port;
        this.username = username;
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getUsername() {
        if (!username.isEmpty())
            return username;
        else return "anonymous";
    }

    public String getPassword() {
        if (!username.isEmpty())
            return password;
        else return "somePassword";
    }

    public void decrypt(String password) {
        this.host = SFDLCryptor.decrypt(password, this.host);
        this.username = SFDLCryptor.decrypt(password, this.username);
        this.password = SFDLCryptor.decrypt(password, this.password);
    }

    @Override
    public String toString() {
        return "ConnectionInfo{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
