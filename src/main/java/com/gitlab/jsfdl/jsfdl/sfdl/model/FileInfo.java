package com.gitlab.jsfdl.jsfdl.sfdl.model;

import com.gitlab.jsfdl.jsfdl.sfdl.SFDLCryptor;

/**
 * Created by oli on 14.05.17.
 */
public class FileInfo {
    private String fileFullPath;
    private String fileName;
    private long fileSize;
    private String directoryPath;
    private String directoryRoot;
    private String fileHash;
    private String fileHashType;
    private String packageName;

    public String getFileFullPath() {
        return fileFullPath;
    }

    public void setFileFullPath(String fileFullPath) {
        this.fileFullPath = fileFullPath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    public void setDirectoryRoot(String directoryRoot) {
        this.directoryRoot = directoryRoot;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }

    public void setFileHashType(String fileHashType) {
        this.fileHashType = fileHashType;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getDirectoryRoot() {
        return directoryRoot;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public FileInfo decrypt(String password) {
        fileFullPath = SFDLCryptor.decrypt(password, fileFullPath);
        fileName = SFDLCryptor.decrypt(password, fileName);
        directoryPath = SFDLCryptor.decrypt(password, directoryPath);
        directoryRoot = SFDLCryptor.decrypt(password, directoryRoot);
        packageName = SFDLCryptor.decrypt(password, packageName);
        return this;
    }
}
