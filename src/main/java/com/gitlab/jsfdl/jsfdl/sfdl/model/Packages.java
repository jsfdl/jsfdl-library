package com.gitlab.jsfdl.jsfdl.sfdl.model;

import java.util.List;

public class Packages {

    private final List<SFDLPackage> sfdlPackages;

    public Packages(List<SFDLPackage> sfdlPackages) {
        this.sfdlPackages = sfdlPackages;
    }

    public int count() {
        return sfdlPackages.size();
    }

    public void decrypt(String password) {
        sfdlPackages.forEach(t -> t.decrypt(password));
    }

    public List<SFDLPackage> getSfdlPackages() {
        return sfdlPackages;
    }
}
