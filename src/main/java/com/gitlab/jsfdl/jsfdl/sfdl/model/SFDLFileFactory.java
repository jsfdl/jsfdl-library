package com.gitlab.jsfdl.jsfdl.sfdl.model;

import com.gitlab.jsfdl.jsfdl.sfdl.xml.converter.SFDLBeanToSFDLFileConverter;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.SFDLBean;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class SFDLFileFactory {
    private static List<SFDLFile> cachedSfdls = new ArrayList<>();
    private final SFDLBeanToSFDLFileConverter converter;

    public SFDLFileFactory(SFDLBeanToSFDLFileConverter converter) {
        this.converter = converter;
    }

    public SFDLFile getSfdlFile(byte[] sfdlFileByteArray) {
        final SFDLFile sfdlFile = createSfdlFile(sfdlFileByteArray);
        Optional<SFDLFile> findAny = cachedSfdls.stream().
                filter((t) -> t.equals(sfdlFile)).findAny();
        if (findAny.isPresent()) {
            return findAny.get();
        }

        cachedSfdls.add(sfdlFile);
        return sfdlFile;
    }

    private SFDLFile createSfdlFile(byte[] sfdlFileByteArray) {
        final File createTempFile;
        try {
            createTempFile = File.createTempFile("jsfdl-", ".sfdl");
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
        try (FileOutputStream fos = new FileOutputStream(createTempFile)) {
            fos.write(sfdlFileByteArray);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
        try {
            final JAXBContext jaxbContext = JAXBContext.newInstance(SFDLBean.class);
            final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            final SFDLBean unmarshal = (SFDLBean) jaxbUnmarshaller.unmarshal(createTempFile);
            return converter.convert(unmarshal);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }

    }
}
