package com.gitlab.jsfdl.jsfdl.sfdl.model;

import com.gitlab.jsfdl.jsfdl.sfdl.SFDLCryptor;

import java.util.List;
import java.util.ListIterator;

public class SFDLPackage {
    private final boolean bulkFolderMode;
    private String bulkFolderPath;
    private String remoteSourceDir;
    private final List<FileInfo> fileInfos;
    private String packageName;
    private List<FileInfo> definedFiles;

    public SFDLPackage(boolean bulkFolderMode, String bulkFolderPath, String remoteSourceDir, List<FileInfo> fileInfos, String packageName) {
        this.bulkFolderMode = bulkFolderMode;
        this.bulkFolderPath = bulkFolderPath;
        this.remoteSourceDir = remoteSourceDir;
        this.fileInfos = fileInfos;
        this.packageName = packageName;
    }


    public boolean isBulkFolderMode() {
        return bulkFolderMode;
    }

    public String getBulkFolderPath() {
        return bulkFolderPath;
    }

    public String getRemoteSourceDir() {
        return remoteSourceDir;
    }

    public List<FileInfo> getFileInfos() {
        return fileInfos;
    }

    public void decrypt(String password) {
            packageName = SFDLCryptor.decrypt(password, packageName);
            bulkFolderPath = SFDLCryptor.decrypt(password, bulkFolderPath);
            remoteSourceDir = SFDLCryptor.decrypt(password, remoteSourceDir);
        for (final ListIterator<FileInfo> i = fileInfos.listIterator(); i.hasNext(); ) {
            final FileInfo element = i.next().decrypt(password);
            i.set(element);
        }

    }

    public String getPackageName() {
        return packageName;
    }

    public List<FileInfo> getDefinedFiles() {
        return definedFiles;
    }
}
