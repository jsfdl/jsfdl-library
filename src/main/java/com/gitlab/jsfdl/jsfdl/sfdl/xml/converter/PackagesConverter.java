package com.gitlab.jsfdl.jsfdl.sfdl.xml.converter;

import com.gitlab.jsfdl.jsfdl.sfdl.model.FileInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.model.Packages;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLPackage;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlFileInfo;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlPackages;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.dto.XmlSFDLPackage;
import com.gitlab.jsfdl.jsfdl.shared.Converter;

import java.util.ArrayList;
import java.util.List;

public class PackagesConverter implements Converter<XmlPackages, Packages> {

    private final Converter<XmlFileInfo, FileInfo> fileInfoConverter;

    public PackagesConverter(Converter<XmlFileInfo, FileInfo> fileInfoConverter) {
        this.fileInfoConverter = fileInfoConverter;
    }

    @Override
    public Packages convert(XmlPackages xmlPackages) {
        final List<SFDLPackage> converted = new ArrayList<>();
        final List<XmlSFDLPackage> packageList = xmlPackages.getPackages();

        for(XmlSFDLPackage pack : packageList){
            converted.add(singleConvert(pack));
        }

        return new Packages(converted);
    }

    private SFDLPackage singleConvert(XmlSFDLPackage pack) {
        final boolean bulkFolderMode = pack.isBulkFolderMode();
        final String bulkFolderPath;
        final String remoteSourceDir;
        final String packageName;
        final List<FileInfo> fileInfos = new ArrayList<>();
        if (bulkFolderMode) {
            //TODO: very ugly...
            bulkFolderPath = pack.getBulkFolderList().get(0).getBulkFolderPath();
            remoteSourceDir = "";
            packageName = pack.getPackagename().isEmpty() ? pack.getBulkFolderList().get(0).getPackageName() : pack.getPackagename();
        } else {
            //TODO: very ugly...
            remoteSourceDir = pack.getXmlFileInfo().get(0).getDirectoryPath();
            packageName = pack.getPackagename();
            final List<XmlFileInfo> collect = pack.getXmlFileInfo();
            collect.forEach(fileInfo1 -> fileInfos.add(fileInfoConverter.convert(fileInfo1)));
            bulkFolderPath = "";
        }
        return new SFDLPackage(bulkFolderMode, bulkFolderPath, remoteSourceDir, fileInfos, packageName);
    }
}
