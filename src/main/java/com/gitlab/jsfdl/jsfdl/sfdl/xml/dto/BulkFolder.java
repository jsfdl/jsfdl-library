package com.gitlab.jsfdl.jsfdl.sfdl.xml.dto;

import com.gitlab.jsfdl.jsfdl.sfdl.SFDLCryptor;

import javax.xml.bind.annotation.XmlElement;

public class BulkFolder {

    @XmlElement(name = "BulkFolderPath")
    private String bulkFolderPath;

    @XmlElement(name = "PackageName")
    private String packageName;

    public String getBulkFolderPath() {
        return bulkFolderPath;
    }

    public String getPackageName() {
        return packageName;
    }

    public void decrypt(String password) {
        this.bulkFolderPath = SFDLCryptor.decrypt(password, this.bulkFolderPath);
        this.packageName = SFDLCryptor.decrypt(password, this.packageName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BulkFolder that = (BulkFolder) o;

        if (bulkFolderPath != null ? !bulkFolderPath.equals(that.bulkFolderPath) : that.bulkFolderPath != null)
            return false;
        return packageName != null ? packageName.equals(that.packageName) : that.packageName == null;
    }

    @Override
    public int hashCode() {
        int result = bulkFolderPath != null ? bulkFolderPath.hashCode() : 0;
        result = 31 * result + (packageName != null ? packageName.hashCode() : 0);
        return result;
    }
}
