package com.gitlab.jsfdl.jsfdl.sfdl.xml.dto;

import com.gitlab.jsfdl.jsfdl.sfdl.SFDLCryptor;

import javax.xml.bind.annotation.XmlElement;

class ConnectionInfoV6 {

    @XmlElement(name = "Host")
    private String host;
    @XmlElement(name = "Port")
    private int port;
    @XmlElement(name = "Username")
    private String username;
    @XmlElement(name = "Password")
    private String password;
    @XmlElement(name = "AuthRequired")
    private boolean authRequired;
    @XmlElement(name = "DataConnectionType")
    private String dataConnection;
    @XmlElement(name = "DataType")
    private String dataType;
    @XmlElement(name = "CharacterEncoding")
    private String characterEncoding;
    @XmlElement(name = "EncryptionMode")
    private String encryptionMode;
    @XmlElement(name = "ListMethod")
    private String listMethod;
    @XmlElement(name = "DefaultPath")
    private String defaultPath;
    @XmlElement(name = "ForceSingleConnection")
    private boolean forceSingleConnection;
    @XmlElement(name = "DataStaleDetection")
    private boolean dataStaleDetection;
    @XmlElement(name = "SpecialServerMode")
    private boolean specialServerMode;

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAuthRequired() {
        return authRequired;
    }

    public String getDataConnection() {
        return dataConnection;
    }

    public String getDataType() {
        return dataType;
    }

    public String getCharacterEncoding() {
        return characterEncoding;
    }

    public String getEncryptionMode() {
        return encryptionMode;
    }

    public String getListMethod() {
        return listMethod;
    }

    public void decrypt(String password) {
        this.host = SFDLCryptor.decrypt(password, this.host);
        this.username = SFDLCryptor.decrypt(password, this.username);
        this.password = SFDLCryptor.decrypt(password, this.password);
        this.defaultPath = SFDLCryptor.decrypt(password, this.defaultPath);
    }
}
