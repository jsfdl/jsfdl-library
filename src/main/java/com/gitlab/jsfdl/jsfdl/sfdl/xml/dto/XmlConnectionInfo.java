package com.gitlab.jsfdl.jsfdl.sfdl.xml.dto;

import javax.xml.bind.annotation.XmlElement;

public class XmlConnectionInfo {

    @XmlElement(name = "Host")
    private String host;
    @XmlElement(name = "Port")
    private int port;
    @XmlElement(name = "Username")
    private String username;
    @XmlElement(name = "Password")
    private String password;
    @XmlElement(name = "AuthRequired")
    private boolean authRequired;
    @XmlElement(name = "DataConnectionType")
    private String dataConnection;
    @XmlElement(name = "DataType")
    private String dataType;
    @XmlElement(name = "CharacterEncoding")
    private String characterEncoding;
    @XmlElement(name = "EncryptionMode")
    private String encryptionMode;
    @XmlElement(name = "ListMethod")
    private String listMethod;
    @XmlElement(name = "DefaultPath")
    private String defaultPath;
    @XmlElement(name = "ForceSingleConnection")
    private boolean forceSingleConnection;
    @XmlElement(name = "DataStaleDetection")
    private boolean dataStaleDetection;
    @XmlElement(name = "SpecialServerMode")
    private boolean specialServerMode;

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAuthRequired() {
        return authRequired;
    }

    public String getDataConnection() {
        return dataConnection;
    }

    public String getDataType() {
        return dataType;
    }

    public String getCharacterEncoding() {
        return characterEncoding;
    }

    public String getEncryptionMode() {
        return encryptionMode;
    }

    public String getListMethod() {
        return listMethod;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        XmlConnectionInfo that = (XmlConnectionInfo) o;

        if (port != that.port) return false;
        if (authRequired != that.authRequired) return false;
        if (forceSingleConnection != that.forceSingleConnection) return false;
        if (dataStaleDetection != that.dataStaleDetection) return false;
        if (specialServerMode != that.specialServerMode) return false;
        if (host != null ? !host.equals(that.host) : that.host != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (dataConnection != null ? !dataConnection.equals(that.dataConnection) : that.dataConnection != null)
            return false;
        if (dataType != null ? !dataType.equals(that.dataType) : that.dataType != null) return false;
        if (characterEncoding != null ? !characterEncoding.equals(that.characterEncoding) : that.characterEncoding != null)
            return false;
        if (encryptionMode != null ? !encryptionMode.equals(that.encryptionMode) : that.encryptionMode != null)
            return false;
        if (listMethod != null ? !listMethod.equals(that.listMethod) : that.listMethod != null) return false;
        return defaultPath != null ? defaultPath.equals(that.defaultPath) : that.defaultPath == null;
    }

    @Override
    public int hashCode() {
        int result = host != null ? host.hashCode() : 0;
        result = 31 * result + port;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (authRequired ? 1 : 0);
        result = 31 * result + (dataConnection != null ? dataConnection.hashCode() : 0);
        result = 31 * result + (dataType != null ? dataType.hashCode() : 0);
        result = 31 * result + (characterEncoding != null ? characterEncoding.hashCode() : 0);
        result = 31 * result + (encryptionMode != null ? encryptionMode.hashCode() : 0);
        result = 31 * result + (listMethod != null ? listMethod.hashCode() : 0);
        result = 31 * result + (defaultPath != null ? defaultPath.hashCode() : 0);
        result = 31 * result + (forceSingleConnection ? 1 : 0);
        result = 31 * result + (dataStaleDetection ? 1 : 0);
        result = 31 * result + (specialServerMode ? 1 : 0);
        return result;
    }
}
