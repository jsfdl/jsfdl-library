package com.gitlab.jsfdl.jsfdl.sfdl.xml.dto;

import com.gitlab.jsfdl.jsfdl.sfdl.SFDLCryptor;

import javax.xml.bind.annotation.XmlElement;

public class XmlFileInfo {

    @XmlElement(name = "FileName")
    private String fileName;

    @XmlElement(name = "DirectoryRoot")
    private String directoryRoot;

    @XmlElement(name = "DirectoryPath")
    private String directoryPath;

    @XmlElement(name = "FileFullPath")
    private String fileFullPath;

    @XmlElement(name = "FileSize")
    private long fileSize;

    @XmlElement(name = "FileHashType")
    private String fileHashType;

    @XmlElement(name = "FileHash")
    private String fileHash;

    @XmlElement(name = "PackageName")
    private String packageName;

    public String getFileName() {
        return fileName;
    }

    public String getDirectoryRoot() {
        return directoryRoot;
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public String getFileFullPath() {
        return fileFullPath;
    }

    public long getFileSize() {
        return fileSize;
    }

    public String getFileHashType() {
        return fileHashType;
    }

    public String getFileHash() {
        return fileHash;
    }

    public String getPackageName() {
        return packageName;
    }

    void decrypt(String password) {
        this.fileName = SFDLCryptor.decrypt(password, this.fileName);
        this.directoryRoot = SFDLCryptor.decrypt(password, this.directoryRoot);
        this.directoryPath = SFDLCryptor.decrypt(password, this.directoryPath);
        this.fileFullPath = SFDLCryptor.decrypt(password, this.fileFullPath);
        this.packageName = SFDLCryptor.decrypt(password, this.packageName);
    }

}
