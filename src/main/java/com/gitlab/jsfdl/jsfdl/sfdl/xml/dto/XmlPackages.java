package com.gitlab.jsfdl.jsfdl.sfdl.xml.dto;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;
import java.util.Objects;

public class XmlPackages {

    @XmlElement(name = "SFDLPackage")
    private List<XmlSFDLPackage> xmlSfdlPackages;

    public List<XmlSFDLPackage> getPackages() {
        return xmlSfdlPackages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XmlPackages xmlPackages = (XmlPackages) o;
        return Objects.equals(xmlSfdlPackages, xmlPackages.xmlSfdlPackages);
    }

    @Override
    public int hashCode() {

        return Objects.hash(xmlSfdlPackages);
    }
}
