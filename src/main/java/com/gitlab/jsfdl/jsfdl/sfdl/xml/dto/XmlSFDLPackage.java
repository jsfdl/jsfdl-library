package com.gitlab.jsfdl.jsfdl.sfdl.xml.dto;

import com.gitlab.jsfdl.jsfdl.sfdl.SFDLCryptor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.Collections;
import java.util.List;

public class XmlSFDLPackage {

    @XmlElement(name = "Packagename")
    private String packagename;

    @XmlElement(name = "BulkFolderMode")
    private boolean bulkFolderMode;

    @XmlElementWrapper(name = "FileList")
    @XmlElement(name = "FileInfo")
    private List<XmlFileInfo> xmlFileInfo;

    @XmlElementWrapper(name = "BulkFolderList")
    @XmlElement(name = "BulkFolder")
    private List<BulkFolder> bulkFolderList;

    public String getPackagename() {
        return packagename;
    }

    public boolean isBulkFolderMode() {
        return bulkFolderMode;
    }

    public List<BulkFolder> getBulkFolderList() {
        return Collections.unmodifiableList(bulkFolderList);
    }

    public List<XmlFileInfo> getXmlFileInfo() {
        return xmlFileInfo;
    }

    public void decrypt(String password) {
        this.packagename = SFDLCryptor.decrypt(password, this.packagename);
        bulkFolderList.stream().forEach(bulkFolder -> bulkFolder.decrypt(password));
        xmlFileInfo.stream().forEach(t -> t.decrypt(password));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        XmlSFDLPackage that = (XmlSFDLPackage) o;

        if (bulkFolderMode != that.bulkFolderMode) return false;
        if (packagename != null ? !packagename.equals(that.packagename) : that.packagename != null) return false;
        if (xmlFileInfo != null ? !xmlFileInfo.equals(that.xmlFileInfo) : that.xmlFileInfo != null) return false;
        return bulkFolderList != null ? bulkFolderList.equals(that.bulkFolderList) : that.bulkFolderList == null;
    }

    @Override
    public int hashCode() {
        int result = packagename != null ? packagename.hashCode() : 0;
        result = 31 * result + (bulkFolderMode ? 1 : 0);
        result = 31 * result + (xmlFileInfo != null ? xmlFileInfo.hashCode() : 0);
        result = 31 * result + (bulkFolderList != null ? bulkFolderList.hashCode() : 0);
        return result;
    }
}
