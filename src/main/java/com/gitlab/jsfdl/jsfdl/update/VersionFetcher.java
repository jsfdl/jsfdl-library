package com.gitlab.jsfdl.jsfdl.update;

import com.gitlab.jsfdl.jsfdl.update.dto.Tag;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class VersionFetcher {

    public List<Tag> loadInfosFromRemote(int projectId) {
        final Type listType = new TypeToken<ArrayList<Tag>>() {
        }.getType();
        String jsonArray = null;
        try {
            String remoteTagUrl = "https://gitlab.com/api/v4/projects/%d/repository/tags";
            InputStream is = new URL(String.format(remoteTagUrl, projectId)).openConnection().getInputStream();
            jsonArray = new Scanner(is, StandardCharsets.UTF_8).useDelimiter("\\Z").next();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Gson().fromJson(jsonArray, listType);
    }

}
