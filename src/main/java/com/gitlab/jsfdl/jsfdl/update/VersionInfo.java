package com.gitlab.jsfdl.jsfdl.update;

import com.gitlab.jsfdl.jsfdl.update.dto.Tag;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

public class VersionInfo {

    private final int current;
    private final List<Tag> tags;

    public VersionInfo(int projectId, int current, final VersionFetcher versionFetcher) {
        this.current = current;
        tags = versionFetcher.loadInfosFromRemote(projectId);
    }

    public boolean updateAvailable() {
        return tags.stream().anyMatch(tag -> tag.getTagAsInt() > current);
    }


    public Tag getUpdateInformations() {
        if (!getLatestTag().isPresent()) {
            throw new NoTagAvailableException();
        }
        int latestTag = getLatestTag().get();
        int finalLatestTag = latestTag;
        return tags.stream().filter(tag -> tag.getTagAsInt() == finalLatestTag).collect(toList()).get(0);

    }

    private Optional<Integer> getLatestTag() {
        Optional<Integer> optional = Optional.empty();
        for (Tag tag : tags) {
            if (!optional.isPresent()) {
                optional = Optional.of(tag.getTagAsInt());
            }
            if (tag.getTagAsInt() > optional.get()) {
                optional = Optional.of(tag.getTagAsInt());
            }
        }
        return optional;
    }

    private static class NoTagAvailableException extends RuntimeException {
    }
}
