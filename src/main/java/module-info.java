module jsfdl {
    requires commons.net;
    requires gson;
    requires java.xml.bind;
    requires io.github.lukehutch.fastclasspathscanner;
    requires java.sql;

    exports com.gitlab.jsfdl.jsfdl.mvp.dateiMenu;
    exports com.gitlab.jsfdl.jsfdl.mvp.download;
    exports com.gitlab.jsfdl.jsfdl.mvp.mainFrame;
    exports com.gitlab.jsfdl.jsfdl.mvp.startstop;
    exports com.gitlab.jsfdl.jsfdl.bus.events.sfdladded;
    exports com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged;
    exports com.gitlab.jsfdl.jsfdl.ftp;
    exports com.gitlab.jsfdl.jsfdl.sfdl.model;
}