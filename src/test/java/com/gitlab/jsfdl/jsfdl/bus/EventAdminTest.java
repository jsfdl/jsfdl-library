package com.gitlab.jsfdl.jsfdl.bus;

import com.gitlab.jsfdl.jsfdl.bus.events.downloadchanged.DownloadChangedEvent;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EventAdminTest {

    @Test
    public void isInstance() {
        final EventAdmin instance = EventAdmin.getInstance();
        final EventAdmin instance2 = EventAdmin.getInstance();
        assertThat(instance, sameInstance(instance2));
    }

    @Test
    public void registerPresenterAndFireEvent() {
        final EventAdmin sut = EventAdmin.getInstance();
        final AbstractMessagePresenter presenter = mock(AbstractMessagePresenter.class);
        final DownloadChangedEvent downloadChangedEvent = mock(DownloadChangedEvent.class);
        when(presenter.eventsToListenFor()).thenReturn(Collections.singleton(DownloadChangedEvent.class));

        sut.registerPresenter(presenter);
        sut.fireEvent(downloadChangedEvent);
//        verify(presenter);
    }

}
