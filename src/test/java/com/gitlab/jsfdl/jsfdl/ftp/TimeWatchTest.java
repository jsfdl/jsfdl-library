package com.gitlab.jsfdl.jsfdl.ftp;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class TimeWatchTest {

    @Test
    void can_count_seconds() throws Exception {
        //given
        TimeWatch sut = TimeWatch.start();
        //when
        Thread.sleep(1010);
        //then
        assertThat(sut.oneSecondPassed(), is(true));
    }

    @Test
    void will_return_false_when_one_second_is_not_passed() {
        //given
        TimeWatch sut = TimeWatch.start();
        //when

        //then
        assertThat(sut.oneSecondPassed(), is(false));
    }
}