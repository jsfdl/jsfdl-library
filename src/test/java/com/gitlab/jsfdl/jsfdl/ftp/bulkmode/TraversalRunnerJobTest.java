package com.gitlab.jsfdl.jsfdl.ftp.bulkmode;

import com.gitlab.jsfdl.jsfdl.ftp.download.FTPDownloadFile;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TraversalRunnerJobTest {

    @Test
    public void does_call_traversalrunner_when_executed() {
        //given
        TraversalRunner traversalRunner = mock(TraversalRunner.class);
        TraversalRunnerJob sut = new TraversalRunnerJob(traversalRunner);
        //when
        final List<FTPDownloadFile> call = sut.call();

        //then
        verify(traversalRunner).getFilesToDownload();
    }
}