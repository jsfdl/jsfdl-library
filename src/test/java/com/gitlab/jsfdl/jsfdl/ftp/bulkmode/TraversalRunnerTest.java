package com.gitlab.jsfdl.jsfdl.ftp.bulkmode;

import com.gitlab.jsfdl.jsfdl.ftp.client.FTPConnection;
import com.gitlab.jsfdl.jsfdl.ftp.client.FTPFileWithFullPath;
import com.gitlab.jsfdl.jsfdl.ftp.download.FTPConnectionPool;
import com.gitlab.jsfdl.jsfdl.ftp.download.FTPDownloadFile;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.ServerOfflineException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class TraversalRunnerTest {

    private final FTPConnectionPool ftpConnectionPool = mock(FTPConnectionPool.class);
    private final FTPConnection ftpConnection = mock(FTPConnection.class);
    TraversalRunner sut = new TraversalRunner("", "", ftpConnectionPool);

    @BeforeEach
    public void setUp() {
        when(ftpConnectionPool.getFreeConnection()).thenReturn(ftpConnection);
    }

    @Test
    public void can_find_files_on_ftp_server() {

        final FTPFileWithFullPath file1 = mock(FTPFileWithFullPath.class);
        final FTPFileWithFullPath file2 = mock(FTPFileWithFullPath.class);
        //given
        when(ftpConnection.getFilesFromDirectory(any())).thenReturn(Arrays.asList(file1, file2));

        //when
        final List<FTPDownloadFile> filesToDownload = sut.getFilesToDownload();
        assertThat(filesToDownload, is(notNullValue()));

    }

    @Test
    public void throws_server_offline_when_server_offline() {
        //given
        //TODO: that is wrong. the constructor throws the exception, not the method getFreeConnection(). atm no time, but better than nothing to test...
        doThrow(ServerOfflineException.class).when(ftpConnectionPool).getFreeConnection();
        //when
        assertThrows(ServerOfflineException.class, () -> {
            sut.getFilesToDownload();
        });
    }

    public void close_connection_pool_after_scanning() {
        //when
        sut.getFilesToDownload();
        //then
        verify(ftpConnectionPool).shutdown();
    }
}