package com.gitlab.jsfdl.jsfdl.ftp.client;

import com.gitlab.jsfdl.jsfdl.sfdl.model.ConnectionInfo;
import org.apache.commons.net.ftp.FTPFile;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.any;

@Disabled
public class FTPConnectionIntegTest {

    private static final int FTP_PORT = 21;
    private static final String host = "ftp.rz.uni-wuerzburg.de";
    final String bulkfolderPath = "/linux/kde/adm";
    private final String path = "/pics";
    private final String pathWithFilename = path + "/00INDEX";
     static FTPFactory ftpFactory;
     static ConnectionInfo connectionInfo;
     static FTPConnection sut;

    @BeforeAll
    public static void setUp() {
        ftpFactory = new MyFTPFactory();
        connectionInfo = new ConnectionInfo(host, FTP_PORT, "anonymous", "top@secret.to");
        sut = new FTPConnection(ftpFactory, connectionInfo);
        try {
            sut.initFTPClient();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void can_list_all_files_on_remote_directory() {
        //when
        final List<FTPFileWithFullPath> files = sut.getFilesFromDirectory(bulkfolderPath);
        //then

        assertThat(files, containsInAnyOrder(new FTPFileWithFullPath(bulkfolderPath, any(FTPFile.class))));
    }

    @Test
    public void can_get_filesize_of_file_on_server() {
        long sizeOf = sut.getSizeOfFile(bulkfolderPath + pathWithFilename);
        assertThat(sizeOf, Matchers.is(296L));
    }

    //    @Test(expected = FileNotFoundException.class)
    @Test
    public void throws_notfoundexception_when_file_does_not_exist() {
        sut.getFilesFromDirectory("/does/not/exist");
    }
}