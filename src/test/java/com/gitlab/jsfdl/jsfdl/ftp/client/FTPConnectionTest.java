package com.gitlab.jsfdl.jsfdl.ftp.client;

import com.gitlab.jsfdl.jsfdl.sfdl.model.ConnectionInfo;
import org.apache.commons.net.ftp.FTPFile;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FTPConnectionTest {

    FTPFactory factory = mock(FTPFactory.class);
    ConnectionInfo connectionInfo = mock(ConnectionInfo.class);

    @Test
    public void name() throws Exception {
        //given
        MyFTPClient ftpClient = mock(MyFTPClient.class);
        when(ftpClient.changeWorkingDirectory("/")).thenReturn(true);
        FTPFile[] ftpFiles = new FTPFile[3];
        ftpFiles[0] = mock(FTPFile.class);
        ftpFiles[1] = mock(FTPFile.class);
        ftpFiles[2] = mock(FTPFile.class);
        when(ftpFiles[0].getName()).thenReturn(".");
        when(ftpFiles[1].getName()).thenReturn("..");
        when(ftpFiles[2].getName()).thenReturn("someFile");
        when(ftpFiles[2].getSize()).thenReturn(20L);
        when(ftpFiles[2].isFile()).thenReturn(true);
        when(ftpClient.listFiles(any())).thenReturn(ftpFiles);

        when(factory.createFTPClient()).thenReturn(ftpClient);
        FTPConnection sut = new FTPConnection(factory, connectionInfo);
        //when
        final List<FTPFileWithFullPath> filesFromDirectory = sut.getFilesFromDirectory("/");

        //then
        assertThat(filesFromDirectory.size(), is(1));
        assertThat(filesFromDirectory.get(0).getRemoteFileLocation(), is("/someFile"));
        assertThat(filesFromDirectory.get(0).getSize(), is(20L));
    }
}