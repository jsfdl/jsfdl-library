package com.gitlab.jsfdl.jsfdl.ftp.download;

import com.gitlab.jsfdl.jsfdl.ftp.client.FTPConnection;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;

import static org.mockito.Mockito.verify;

public class DownloadJobTest {

    @Test
    public void can_execute_download_on_connection() {
        FTPConnection ftpConnection = Mockito.mock(FTPConnection.class);
        String remoteFile = "";
        File localFile = Mockito.mock(File.class);
        //given
        DownloadJob sut = new DownloadJob(ftpConnection, remoteFile, localFile);
        //when
        sut.call();
        //then
        verify(ftpConnection).download(remoteFile, localFile);
    }
}