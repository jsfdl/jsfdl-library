package com.gitlab.jsfdl.jsfdl.ftp.download;

import com.gitlab.jsfdl.jsfdl.ftp.client.FTPConnection;
import com.gitlab.jsfdl.jsfdl.ftp.client.FTPFactory;
import com.gitlab.jsfdl.jsfdl.ftp.client.MyFTPClient;
import com.gitlab.jsfdl.jsfdl.sfdl.model.ConnectionInfo;
import com.gitlab.jsfdl.jsfdl.shared.exceptions.ServerOfflineException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.NoRouteToHostException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class FTPConnectionPoolTest {

    private FTPFactory ftpfactoryMock = mock(FTPFactory.class);
    private MyFTPClient ftpClientMock = mock(MyFTPClient.class);
    private ConnectionInfo connectionInfoMock = mock(ConnectionInfo.class);

    @BeforeEach
    void setUp() throws Exception {
        when(ftpClientMock.login(null, null)).thenReturn(true);
        when(ftpfactoryMock.createFTPClient()).thenReturn(ftpClientMock);
        when(connectionInfoMock.getHost()).thenReturn("localhost");
        when(connectionInfoMock.getPort()).thenReturn(2000);
    }

    @Test
    void should_get_a_connection() {
        //given
        FTPConnectionPool sut = new FTPConnectionPool(3, ftpfactoryMock, connectionInfoMock);
        sut.initialize();
        //when
        final FTPConnection freeConnection = sut.getFreeConnection();

        //then
        assertThat(freeConnection, is(notNullValue()));
        assertThat(sut.connections(), is(2));
    }

    @Test
    void should_release_a_connection() {
        //given
        FTPConnectionPool sut = new FTPConnectionPool(3, ftpfactoryMock, connectionInfoMock);
        sut.initialize();
        //when
        final FTPConnection freeConnection = sut.getFreeConnection();
        sut.releaseConnection(freeConnection);

        //then
        assertThat(sut.connections(), is(3));
    }

    @Test
    void should_shutdown_all_connections() throws Exception {
        //given
        FTPConnectionPool sut = new FTPConnectionPool(3, ftpfactoryMock, connectionInfoMock);
        sut.initialize();
        //when
        sut.shutdown();

        //then
        verify(ftpClientMock, times(3)).logout();
    }

    @Test
    void should_throw_server_offline_on_ftp_init_error() throws IOException {
        //given
        doThrow(NoRouteToHostException.class).when(ftpClientMock).connect(anyString(), anyInt());
        FTPConnectionPool sut = new FTPConnectionPool(3, ftpfactoryMock, connectionInfoMock);

        //when
        assertThrows(ServerOfflineException.class, sut::initialize);

        //then
        //expect exception
    }
}