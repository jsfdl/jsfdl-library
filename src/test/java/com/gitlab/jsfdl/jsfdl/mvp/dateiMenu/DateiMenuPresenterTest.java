package com.gitlab.jsfdl.jsfdl.mvp.dateiMenu;

import org.junit.jupiter.api.BeforeAll;
import org.mockito.Mockito;

public class DateiMenuPresenterTest {

    DateiMenuMvp.Model model;
    DateiMenuMvp.View view;
    DateiMenuPresenter presenter;

    @BeforeAll
    public void setUp() {
        model = Mockito.mock(DateiMenuMvp.Model.class);
        view = Mockito.mock(DateiMenuMvp.View.class);
        presenter = new DateiMenuPresenter(model, view);
    }

//    @Test
//    public void removes_starting_slash() throws Exception {
//        SFDLFile sfdlFile = new SFDLFile(new File(SFDLFileTest.class.getResource("12monkeys.xml").getFile()));
//        presenter.fireEvent(new SFDLFileAdded(sfdlFile));
//        verify(model).addedSfdl(sfdlFile);
//        verify(view).showSFDLAddedDialog(sfdlFile);
//    }
}
