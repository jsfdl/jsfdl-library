package com.gitlab.jsfdl.jsfdl.mvp.startstop;

import com.gitlab.jsfdl.jsfdl.mvp.startstop.StartStopMvp.Model;
import com.gitlab.jsfdl.jsfdl.mvp.startstop.StartStopMvp.View;
import org.junit.jupiter.api.BeforeAll;
import org.mockito.Mockito;

public class StartStopPresenterTest {

    Model startStopModel;
    View startStopView;
    StartStopPresenter startStopPresenter;

    @BeforeAll
    public void setUp() {
        startStopModel = Mockito.mock(Model.class);
        startStopView = Mockito.mock(View.class);
        startStopPresenter = new StartStopPresenter(startStopView, startStopModel);
    }

}
