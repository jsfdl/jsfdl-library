package com.gitlab.jsfdl.jsfdl.sfdl;

import org.apache.ftpserver.ftplet.FileSystemView;
import org.apache.ftpserver.ftplet.FtpFile;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class ResourceFileSystemView implements FileSystemView {

    private final FtpFile userHome = new FtpFile() {
        @Override
        public String getAbsolutePath() {
            return getClass().getResource("").getPath();
        }

        @Override
        public String getName() {
            return "root";
        }

        @Override
        public boolean isHidden() {
            return false;
        }

        @Override
        public boolean isDirectory() {
            return true;
        }

        @Override
        public boolean isFile() {
            return false;
        }

        @Override
        public boolean doesExist() {
            return true;
        }

        @Override
        public boolean isReadable() {
            return true;
        }

        @Override
        public boolean isWritable() {
            return true;
        }

        @Override
        public boolean isRemovable() {
            return true;
        }

        @Override
        public String getOwnerName() {
            return "owner";
        }

        @Override
        public String getGroupName() {
            return "group";
        }

        @Override
        public int getLinkCount() {
            return 0;
        }

        @Override
        public long getLastModified() {
            return 0;
        }

        @Override
        public boolean setLastModified(long time) {
            return false;
        }

        @Override
        public long getSize() {
            return 0;
        }

        @Override
        public Object getPhysicalFile() {
            return null;
        }

        @Override
        public boolean mkdir() {
            return false;
        }

        @Override
        public boolean delete() {
            return false;
        }

        @Override
        public boolean move(FtpFile destination) {
            return false;
        }

        @Override
        public List<? extends FtpFile> listFiles() {
            return null;
        }

        @Override
        public OutputStream createOutputStream(long offset) {
            return null;
        }

        @Override
        public InputStream createInputStream(long offset) {
            return null;
        }
    };
    private FtpFile currentDir;

    public ResourceFileSystemView() {
        this.currentDir = userHome;
    }

    @Override
    public FtpFile getHomeDirectory() {
        return userHome;
    }

    @Override
    public FtpFile getWorkingDirectory() {
        return currentDir;
    }

    @Override
    public boolean changeWorkingDirectory(String dir) {
        System.out.println("want to change to " + dir);
        return new File(userHome + "/" + dir).exists();
    }

    @Override
    public FtpFile getFile(String file) {
        return userHome;
    }

    @Override
    public boolean isRandomAccessible() {
        return false;
    }

    @Override
    public void dispose() {

    }
}
