package com.gitlab.jsfdl.jsfdl.sfdl;

import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLFile;
import com.gitlab.jsfdl.jsfdl.sfdl.model.SFDLFileFactory;
import com.gitlab.jsfdl.jsfdl.sfdl.xml.converter.SFDLBeanToSFDLFileConverter;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class SFDLEntryFactoryTest {

    private final SFDLBeanToSFDLFileConverter sfdlBeanToSFDLFileConverter = Mockito.mock(SFDLBeanToSFDLFileConverter.class);

    @Test
    public void returns_same_instance_when_same_file_is_added() throws Exception {
        //given
        final URL resource = getClass().getResource("12monkeys_sfdlv6.xml");
        final byte[] readAllBytes = Files.readAllBytes(Paths.get(resource.toURI()));
        final SFDLFile origSfdlFile = Mockito.mock(SFDLFile.class);
        when(sfdlBeanToSFDLFileConverter.convert(any())).thenReturn(origSfdlFile);
        final SFDLFileFactory sut = new SFDLFileFactory(sfdlBeanToSFDLFileConverter);
        //when
        final SFDLFile sfdlFile = sut.getSfdlFile(readAllBytes);
        final SFDLFile sfdlFile2 = sut.getSfdlFile(readAllBytes);
        //then
        assertThat(sfdlFile, is(sameInstance(sfdlFile2)));
    }
}